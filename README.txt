Dynamic Columns

The module allows to organize the layout of specified HTML element into dynamic columns. 
Admins can specify maximum count of columns and minimum width of one column. Content will be reformatted dynamicaly depending on size of browser window

To install, place the entire module folder into your modules directory.
Go to administer -> site building -> modules and enable the Dynamic Columns module.

To format content of some element in dynamic columns go to 
Administer -> Site configuration -> Dynamic columns.
Define elements for multicolumn layout in format: 
.some-class .another-class:300:3, 
where:
".some-class .another-class" - css selector of parent elements, 
300 - minimum width of column in px, 
3 - max count of columns. 
Each definition must be on separate line