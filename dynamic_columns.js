// Global killswitch: only run if we are in a supported browser.
if (Drupal.jsEnabled) {
  function dynamic_columns(selector, minwidth, columns) {
    $(selector).each(function() {
      var parentelem = $(this);
       
      if (parentelem.children(".dynamic-columns-column-wrapper").length == 0) {
        parentelem.children().wrapAll("<div class=\"dynamic-columns-column-wrapper clear-block\"></div>");
      }
      parentelem = parentelem.children(".dynamic-columns-column-wrapper").eq(0);
       
      var colscount = 1;
      for (var i = columns; i > 1; i--) {
        if (parentelem.width() >= minwidth * i) {
          colscount = i;
          break;
        }
      }
      parentelem.attr("class", "");
      parentelem.addClass("dynamic-columns-column-wrapper");
      parentelem.addClass("clear-block");
      parentelem.addClass("dynamic-columns-column-wrapper-" + colscount +"-cols");
       
      if (parentelem.children(".dynamic-columns-column").length != colscount) {
        parentelem.children(".dynamic-columns-column").each(function() {
          $(this).replaceWith($(this).children());
        }
        );
         
        var itemscount = Math.ceil(parentelem.children().length / colscount);
        var newwidth = ((100 / colscount) - (1/colscount)) + "%";
        var set = parentelem.children();
        for(var j = 0; j < colscount; j++) {
          var col = set.slice(j * itemscount, j * itemscount + itemscount);
          if (col.length) {
            col.wrapAll("<div class=\"dynamic-columns-column dynamic-columns-column-num-" + j + "\" style=\"width:" + newwidth + ";float:left;vertical-align:top;\"></div>");
          }
        }
      }
    }
    );
  }
}